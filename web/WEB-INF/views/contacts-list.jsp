<%--
  Created by IntelliJ IDEA.
  User: ssylla
  Date: 07/01/2020
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Gestion des Contacts - Liste des contacts</title>
		<link rel="stylesheet" type="text/css"
		      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
		<link rel="stylesheet" type="text/css"
		      href="<c:url value="/vendor/foundation-6.5.1/css/foundation.min.css" />" />
		<link rel="stylesheet" href="<c:url value="/css/style.css"/>" />
	</head>
	<body>
		<div class="callout large primary">
			<div class="row column text-center">
				<h1>Gestion des contacts - Liste des contacts</h1>
			</div>
		</div>
		<div class="row small-5 small-centered">
			<jsp:useBean id="contactBean" class="model.ContactBean" scope="request"></jsp:useBean>
			<table>
				<thead>
					<tr>
						<th>Nom</th>
						<th>Prénom</th>
						<th>Email</th>
						<th class="text-center">Actions</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="contact" items="${contactBean.contacts}">
						<tr>
							<td>${contact.lastName}</td>
							<td>${contact.firstName}</td>
							<td>${contact.email}</td>
							<td class="text-center">
								<a class="button" href="<c:url value="/contacts/edit?id=${contact.id}" />"><i class="fa fa-edit"></i></a>
								<a class="button alert" href="<c:url value="/contacts/delete?id=${contact.id}" />"><i class="fa fa-user-times"></i></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="text-right">
				<a class="button align-right" href="<c:url value="/contacts/new" />"><i class="fa fa-user-plus"></i> Ajouter</a>
			</div>
		</div>
		<script src="${pageContext.request.contextPath}/vendor/foundation-6.5.1/js/vendor/jquery.js"></script>
		<script src="${pageContext.request.contextPath}/vendor/foundation-6.5.1/js/vendor/foundation.min.js"></script>
		<script>
			$(document).foundation();
			document.documentElement.setAttribute('data-useragent', navigator.userAgent);
		</script>
	</body>
</html>
