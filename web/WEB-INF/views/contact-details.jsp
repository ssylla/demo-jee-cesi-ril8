<%--
  Created by IntelliJ IDEA.
  User: ssylla
  Date: 07/01/2020
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Gestion des Contacts - Liste des contacts</title>
		<link rel="stylesheet" type="text/css"
		      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
		<link rel="stylesheet" type="text/css"
		      href="<c:url value="/vendor/foundation-6.5.1/css/foundation.min.css" />" />
		<link rel="stylesheet" href="<c:url value="/css/style.css"/>" />
	</head>
	<body>
		<jsp:useBean id="contactBean" class="model.ContactBean" scope="request"></jsp:useBean>
		<div class="callout large primary">
			<div class="row column text-center">
				<h1>Gestion des contacts - Information du contact</h1>
			</div>
		</div>
		<div class="row small-5 small-centered">
			
			<c:if test="${!empty contactBean.exception.message}">
				<div class="callout alert">
					<p>${contactBean.exception.message}</p>
				</div>
			</c:if>
			
			<form method="POST">
				<input type="hidden" value="${contactBean.currentContact.id}" name="form-id"/>
				<div class="form-icons">
					<h4>Fiche Contact</h4>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fas fa-user-ninja"></i>
						</span>
						<input class="input-group-field" type="text" placeholder="Nom" name="form-last-name"
						       value="${contactBean.currentContact.lastName}"/>
					</div>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fa user-tie"></i>
						</span>
						<input class="input-group-field" type="text" placeholder="Prénom" name="form-first-name"
						       value="${contactBean.currentContact.firstName}"/>
					</div>
					<div class="input-group">
						<span class="input-group-label">
							<i class="fa fa-envelope"></i>
						</span>
						<input class="input-group-field" type="text" placeholder="Email" name="form-email"
						       value="${contactBean.currentContact.email}"/>
					</div>
				</div>
				<button class="button expanded">Valider</button>
			</form>
		</div>
		<script src="${pageContext.request.contextPath}/vendor/foundation-6.5.1/js/vendor/jquery.js"></script>
		<script src="${pageContext.request.contextPath}/vendor/foundation-6.5.1/js/vendor/foundation.min.js"></script>
		<script>
			$(document).foundation();
			document.documentElement.setAttribute('data-useragent', navigator.userAgent);
		</script>
	</body>
</html>
