package exception;

public class ContactFormException extends Exception {
	
	public ContactFormException( String message ) {
		super( message );
	}
}
