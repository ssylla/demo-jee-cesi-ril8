package model;

import bo.Contact;
import exception.ContactFormException;
import exception.ContactNotFoundException;
import util.StringUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ContactBean implements Serializable {
	
	private static final String CONTACTS_LIST_SESSION_KEY = "contactsList";
	private static final Set<Contact> DEFAULT_CONTACTS_LIST = new HashSet<>();
	private static final String FORM_FIELD_ID = "form-id";
	private static final String FORM_FIELD_LAST_NAME = "form-last-name";
	private static final String FORM_FIELD_FIRST_NAME = "form-first-name";
	private static final String FORM_FIELD_EMAIL = "form-email";
	
	static {
		DEFAULT_CONTACTS_LIST.add( new Contact( "S", "Séga", "ssy@ss.org" ) );
	}
	
	private Contact currentContact;
	private Set<Contact> contacts;
	private ContactFormException exception;
	
	public ContactBean() {}
	
	public ContactBean( HttpServletRequest request ) {
		
		HttpSession session = request.getSession( true );
		contacts = ( Set<Contact> ) session.getAttribute( CONTACTS_LIST_SESSION_KEY );
		if ( null == contacts ) {
			contacts = DEFAULT_CONTACTS_LIST.stream().collect( Collectors.toSet() );
			session.setAttribute( CONTACTS_LIST_SESSION_KEY, contacts );
		}
	}
	
	public void setCurrentContact( String id ) throws ContactNotFoundException {
		currentContact = null;
		if ( StringUtil.isEmpty( id ) || null == contacts )
			throw new ContactNotFoundException( "Contact non trouvé!!!" );
		// for ( Contact contact : contacts ) {
		// 	if ( id.equals( contact.getId() ) ) {
		// 		currentContact = contact;
		// 		break;
		// 	}
		// }
		
		currentContact = contacts.stream()
								 .filter( contact -> id.equals( contact.getId() ) )
								 .findAny()
								 .orElse( null );
		
		if ( currentContact == null ) throw new ContactNotFoundException( "Contact non trouvé!!!" );
	}
	
	public void setContactFromForm( HttpServletRequest request ) throws ContactFormException {
		
		String id = request.getParameter( FORM_FIELD_ID );
		String lastName = request.getParameter( FORM_FIELD_LAST_NAME );
		String firstName = request.getParameter( FORM_FIELD_FIRST_NAME );
		String email = request.getParameter( FORM_FIELD_EMAIL );
		
		if ( StringUtil.isEmpty( id )) {
			currentContact = new Contact( lastName, firstName, email );
		} else {
			try {
				setCurrentContact( id );
				currentContact.setLastName( lastName );
				currentContact.setFirstName( firstName );
				currentContact.setEmail( email );
			} catch ( ContactNotFoundException e ) {
				currentContact = new Contact( lastName, firstName, email );
			}
		}
		
		if ( StringUtil.isEmpty( email ) ) {
			exception = new ContactFormException( "Email est obligatoire !!" );
			throw exception;
		} else {
			contacts.add( currentContact );
		}
	}
	
	public void removeContact(String id) throws ContactNotFoundException {
		
		setCurrentContact( id );
		contacts.remove( currentContact );
		currentContact = null;
	}
	
	public Contact getCurrentContact() {
		return currentContact;
	}
	
	public void setCurrentContact( Contact currentContact ) {
		this.currentContact = currentContact;
	}
	
	public Set<Contact> getContacts() {
		return contacts;
	}
	
	public void setContacts( Set<Contact> contacts ) {
		this.contacts = contacts;
	}
	
	public ContactFormException getException() {
		return exception;
	}
	
	public void setException( ContactFormException exception ) {
		this.exception = exception;
	}
}
