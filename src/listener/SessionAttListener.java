package listener;

import org.apache.taglibs.standard.lang.jstl.LessThanOrEqualsOperator;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebListener
public class SessionAttListener implements HttpSessionAttributeListener {
	
	private static final Logger LOGGER = Logger.getLogger( SessionAttListener.class.getName() );
	
	@Override
	public void attributeAdded( HttpSessionBindingEvent se ) {
		LOGGER.log( Level.INFO, "On vient d'ajouter : {0} avec la valeur {1}", new String[]{se.getName(), se.getValue().toString()} );
	}
	
	@Override
	public void attributeRemoved( HttpSessionBindingEvent se ) {
		LOGGER.log( Level.INFO, "On vient de supprimer : {0}", se.getName() );
	}
	
	@Override
	public void attributeReplaced( HttpSessionBindingEvent se ) {
		LOGGER.log( Level.INFO, "On vient de modifier : {0} avec la valeur {1}", new String[]{se.getName(), se.getValue().toString()} );
	}
}
