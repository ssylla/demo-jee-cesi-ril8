package listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebListener
public class RequestListener implements ServletRequestListener {
	
	private static final Logger LOGGER = Logger.getLogger( RequestListener.class.getName() );
	
	@Override
	public void requestDestroyed( ServletRequestEvent sre ) {
		long duration = System.currentTimeMillis() - (Long) sre.getServletRequest().getAttribute( "start" );
		LOGGER.log( Level.INFO, "Fin de la requête en : {0} ms...", duration );
	}
	
	@Override
	public void requestInitialized( ServletRequestEvent sre ) {
		LOGGER.log( Level.INFO, "Démarrage de la requête..." );
		sre.getServletRequest().setAttribute( "start", System.currentTimeMillis() );
	}
}
