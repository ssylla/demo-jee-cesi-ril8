package listener;

import org.apache.taglibs.standard.lang.jstl.LessThanOrEqualsOperator;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebListener
public class SessionListener implements HttpSessionListener {
	
	private static final Logger LOGGER = Logger.getLogger( SessionListener.class.getName() );
	private int count = 0;
	@Override
	public void sessionCreated( HttpSessionEvent sessionEvent ) {
		
		LOGGER.log( Level.INFO, "Nouvelle session : nb sessions = {0}", ++count );
	}
	
	@Override
	public void sessionDestroyed( HttpSessionEvent sessionEvent ) {
		LOGGER.log( Level.INFO, "Fin d'une session : nb sessions = {0}", --count );
	}
}
