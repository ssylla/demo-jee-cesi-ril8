package bo;

import java.io.Serializable;
import java.util.Date;

public class Request implements Serializable {
	
	private String id;
	private String path;
	private int duration;
	private Date date;
	
	public Request() {}
	
	public Request( String path, int duration, Date date ) {
		this.path = path;
		this.duration = duration;
		this.date = date;
	}
	
	public Request( String id, String path, int duration, Date date ) {
		this.id = id;
		this.path = path;
		this.duration = duration;
		this.date = date;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId( String id ) {
		this.id = id;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath( String path ) {
		this.path = path;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration( int duration ) {
		this.duration = duration;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate( Date date ) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Request{" );
		sb.append( "id='" ).append( id ).append( '\'' );
		sb.append( ", path='" ).append( path ).append( '\'' );
		sb.append( ", duration=" ).append( duration );
		sb.append( ", date=" ).append( date );
		sb.append( '}' );
		return sb.toString();
	}
}
