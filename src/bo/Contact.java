package bo;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Contact implements Serializable {
	
	private String id;
	private String lastName;
	private String firstName;
	private String email;
	
	public Contact() {
		id = UUID.randomUUID().toString();
	}
	
	public Contact( String lastName, String firstName, String email ) {
		this();
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
	}
	
	public Contact( String id, String lastName, String firstName, String email ) {
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId( String id ) {
		this.id = id;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName( String lastName ) {
		this.lastName = lastName;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName( String firstName ) {
		this.firstName = firstName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail( String email ) {
		this.email = email;
	}
	
	@Override
	public boolean equals( Object o ) {
		if ( this == o ) return true;
		if ( !(o instanceof Contact) ) return false;
		Contact contact = ( Contact ) o;
		return Objects.equals( id, contact.id );
	}
	
	@Override
	public int hashCode() {
		return Objects.hash( id );
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Contact{" );
		sb.append( "id=" ).append( id );
		sb.append( ", lastName='" ).append( lastName ).append( '\'' );
		sb.append( ", firstName='" ).append( firstName ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
