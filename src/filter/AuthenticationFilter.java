package filter;

import controller.LoginController;
import model.LoginBean;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = {"/home", "/contacts", "/contacts/*"})
public class AuthenticationFilter implements Filter {
	@Override
	public void doFilter( ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain ) throws IOException, ServletException {
		
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		LoginBean model = new LoginBean();
		if ( !model.isAuthenticated( request ) ) {
			((HttpServletResponse)servletResponse).sendRedirect( request.getContextPath() + LoginController.LOGIN_URL_PATTERN );
		} else {
			filterChain.doFilter( servletRequest, servletResponse );
		}
	}
}
