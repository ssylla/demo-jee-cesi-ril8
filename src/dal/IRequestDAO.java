package dal;

import bo.Request;

import java.util.HashSet;

public interface IRequestDAO extends IDAO<String, Request> {
	
	HashSet getStats();
}
