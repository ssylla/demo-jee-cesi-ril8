package dal.jpa;

import bo.User;
import dal.IUserDAO;

import java.sql.SQLException;
import java.util.Collection;

public class JPAUserDAO implements IUserDAO {
	@Override
	public User authenticate( String login, String password ) throws SQLException {
		return null;
	}
	
	@Override
	public void create( User object ) throws SQLException {
	
	}
	
	@Override
	public void update( User object ) {
	
	}
	
	@Override
	public void deleteById( String s ) {
	
	}
	
	@Override
	public void delete( User object ) {
	
	}
	
	@Override
	public User findById( String s ) {
		return null;
	}
	
	@Override
	public Collection<User> findAll() {
		return null;
	}
}
