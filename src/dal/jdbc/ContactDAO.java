package dal.jdbc;

import bo.Contact;
import dal.DAOFactory;
import dal.IContactDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collection;

public class ContactDAO implements IContactDAO {
	
	private static final String INSERT_QUERY = "INSERT INTO contact (name, email) VALUES (?, ?)";
	
	@Override
	public void create( Contact contactACreer ) throws SQLException {
		
		try( Connection connection = DAOFactory.getJDBCConnection();
			 PreparedStatement ps = connection.prepareStatement( INSERT_QUERY ) ) {
			
			ps.setString( 1, contactACreer.getLastName() );
			ps.setString( 2, contactACreer.getEmail() );
			
			ps.executeUpdate();
		}
	}
	
	@Override
	public void update( Contact object ) {
		//TODO à faire l'éval
	}
	
	@Override
	public void deleteById( String s ) {
		//TODO à faire l'éval
	}
	
	@Override
	public void delete( Contact object ) {
		//TODO à faire l'éval
	}
	
	@Override
	public Contact findById( String s ) {
		//TODO à faire l'éval
		return null;
	}
	
	@Override
	public Collection<Contact> findAll() {
		//TODO à faire l'éval
		return null;
	}
}
