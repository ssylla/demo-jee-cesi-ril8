package controller;

import exception.ContactFormException;
import exception.ContactNotFoundException;
import model.ContactBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet( name = "contactController", urlPatterns = {"/home", "/contacts", "/contacts/new", "/contacts/edit", "/contacts/delete"} )
public class ContactController extends HttpServlet {
	
	private static final String CONTACTS_LIST_JSP = "/WEB-INF/views/contacts-list.jsp";
	private static final String CONTACT_DETAILS_JSP = "/WEB-INF/views/contact-details.jsp";
	private static final String HOME_URL_PATTERN = "/contacts";
	
	@Override
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		
		ContactBean contactBean = ( ContactBean ) request.getAttribute( "contactBean" );
		if ( null == contactBean ) {
			contactBean = new ContactBean( request );
			request.setAttribute( "contactBean", contactBean );
		}
		String path = request.getServletPath();
		switch ( path ) {
			case "/contacts/delete":
				try {
					contactBean.removeContact( request.getParameter( "id" ) );
				} catch ( ContactNotFoundException e ) {}
				response.sendRedirect( request.getContextPath() + HOME_URL_PATTERN );
				break;
			case "/contacts/edit":
				try {
					contactBean.setCurrentContact( request.getParameter( "id" ) );
				} catch ( ContactNotFoundException e ) {
					response.sendRedirect( request.getContextPath() + HOME_URL_PATTERN );
					break;
				}
			case "/contacts/new":
				request.getServletContext().getRequestDispatcher( CONTACT_DETAILS_JSP ).forward( request, response );
				break;
			default:
				request.getServletContext().getRequestDispatcher( CONTACTS_LIST_JSP ).forward( request, response );
		}
	}
	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		
		ContactBean contactBean = new ContactBean( request );
		try {
			contactBean.setContactFromForm( request );
			response.sendRedirect( request.getContextPath() + HOME_URL_PATTERN );
		} catch ( ContactFormException e ) {
			request.setAttribute( "contactBean", contactBean );
			doGet( request, response );
		}
	}
}
